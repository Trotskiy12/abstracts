# Классы и прототипы

es6 синтаксис. -> class ...

JS не ООП язык. Раньше классы создавались через функции.

**Классы** позволяют создавать **прототипы** для объектов. На основании прототипов создаётся **экземпляр** класса. **Экземпляры** могу иметь собственные свойства и методы. **Экземпляры** наследуют свойства и методы прототипов.

```javascript
class Comment {
    constructor(text) {
        this.text = text;
        this.qty = 0
    }

    upvote() {
        this.qty += 1;
    }
}

const firstComment = new Comment('random text');
```
this - указывает на экземпляр класса. Внутри конструктора инициализируем свойства для каждого экземляра класса.

new - вызывает конструктор.

у firstComment появяться собственный свойства экземпляра + Prototype: Object внутри будет метод upvote.

# Наследование по цепочке
(Наследование)
Свойства и методы экземпляра -> Свойства и методы класса -> Все методы и свойства класса Object

Цепочка прототипов: firstComment -> Comment -> Object.
Comment
    - text
    - qty
    class Comment
        - constructor: class Comment
        - upvote: f upvote()
        Object
        - constructor: f Object()

# Проверка принадлежности классу

Проверка на принадлежность экземпляра к классу -> **instanceof** Class.

firstComment instanceof Comment // true
firstComment instanceof Object // true

# Вызов методов

Для каждого экземпляра класса можно вызвать методы класса, используя dot notation.
Методы можно вызывать многократно.

# Проврека принадлежности свойств экземпляру объекта

```javascript
    const firstComment = new Comment('First comment')
    firstComment.hasOwnProperty('text') // true
    firstComment.hasOwnProperty('qty') // true
    firstComment.hasOwnProperty('upvote') // false
```

upvote -> false, потому что upvote не собсвенный метод firstComment.

# Статические методы

Создание статических методов с помощью static.
Этот метод доступен как свойство класса и **и не наследуется** экземплярами класса.

```javascript
class Comment {
    ...
    static mergeComments(first, second) {
        return `${first} ${second}`;
    }
}

Comment.mergeComments('First', 'Second'); 
```

# Расширение других классов 

extends

```javascript
class NumbersArray extends Array {
    // Родительский конструктор вызывается автоматически
    sum() {
        return this.reduce((el, acc) => acc += el, 0)
    }
}

const myArray = new NumbersArray(2,5,7)
console.log(myArray)
myArray.sum()
```

Цепочка прототипов: myArray => NumbersArray => Array => Object

# Что же такое прототип?

__proto__ - скрытое свойство. (в ручную не рекомендуется изменять)

# Строки и числа ведут себя как объекты

Из-за наследования от экзепляра String/Number, которые в свою очередь, наследуются от Object. То и числа и строки ведут себя как объекты.