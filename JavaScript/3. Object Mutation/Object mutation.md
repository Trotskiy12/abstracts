# Мутация в JS

- Для примитивных типов копирование происходит по значению.
    ```javascript
        const a = 10;
        let b = a;
        b = 30;
        console.log(a) // 10
        console.log(b) // 30 
    ```
- Для ссылочного типа:
    ```javascript
        const person = {
            name: 'test',
            age: 21
        }
        person.age = 22;
        person.isAdult = true;
        console.log(person.age) // 22
        console.log(person.isAdult) // true
    ```
    Мы можем менять и добавлять свойства в объект -- мутирование. Они доступно потому, что переменная person хранит ссылку на объект.

- Мутирование копий
    ```javascript
        const person = {
            name: 'Bob',
            age: 25
        }
        const person2 = person; // copy by reference

        person2.age = 26
        person2.isAdult = true
        console.log(person.age) // 22
        console.log(person.isAdult) // true
    ```

    Учитывая то, что в person2 объекта содержится **поверхностная копия** person, при изменении свойств у person2, они будут также изменяться у person.

## Как избежать мутаций?
Вариант 1. (Если у объекта, **нет** вложенных объектов)
```javascript
const person = {
    name: 'Bob',
    age: 22
}
cosnt person2 = Object.assign({}, person);
person2.age = 26;
console.log(person2.age) // 26
console.log(person.age) // 25
```

- Object - это глобальный объект. (прототип для всех объектов) 
- assign - это метод, для создания нового объекта. То есть assing создаст новый объект, со всеми свойстваи person

В таком случае, в person2 будет новая ссылка на объект, отличная от person, в следствии этого изменение свойств у person2 не повлечёт изменений в person.

НО, если у объекта есть вложенные объекты, то ссылки на них **сохраняются**

Вариант 2. (Использование spread-оператора)
```javascript
const person = {
    name: 'Bob',
    age: 25
}

const person2 = { ...person } // разделение объекта на свойства

person2.name = 'Vova'

console.log(person2.name) // Vova
console.log(person.name) // Bob
```

Использование spread-оператора -- не решает проблему с вложенными объектами. Ссылки вложенные объекты **сохраняются**

Вариант 3. (копия объекта, через JSON)

```javascript
const person = {
    name: 'Bob',
    age: 25
}

const person2 = JSON.parse(JSON.stringify(person))

person2.name = 'Vova'

console.log(person2.name) // Vova
console.log(person.name) // Bob
```

Конвертируем объект в JSON, затем JSON конвертируем в объект. Также можем менять свойства в новом объекте, без мутации "старого".

С помощью двойной конвертации - ссылки на вложеные объекты **не сохраняются.**

