# Обработка ошибок

```javascript
    const fnWithError = () => {
        throw new Error('some error')
    }

    fnWithError() // после выводится ошибка, код дальше не просматривается

    console.log('next...')
```

Uncaught Error - не пойманная ошибка

Как их отлавливать?
Try/Catch
```javascript
    try {
        // выполнение блока кода
    } catch (error) {
        // Этот блок выполняется в случае возникновения ошибок в блоке try
    }
```

```javascript
const fnWithError = () => {
    throw new Error('some error')
}

try {
    fnWithError()
} catch (error) {
    console.error(error);
    console.log(error.message)
}

console.log('Continue...')
```