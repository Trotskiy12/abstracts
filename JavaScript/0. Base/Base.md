# База
1. Самое основное в JS: Выражения, Функции, Объекты.
2. Главная идея JS: Практически все сущности в javascript -- **объекты**.
3. Объект - это набор свойств "name: value".
    ```javascript
    {
        visible: true,
        color: red;
        title: 'Title'
        subObject: {
            value: 1,
            id: 10
        }
    }
    ```
4. Массив - это объект. Функция, строка, число (ведут себя как объекты).
## Выражение console.log()
```javascript
    console.log('Hello world');
```
 - console - объект.
 - log - метод (свойство).
 - . - точечная запись. Можно получить доступ к свойству объекта.
 - log() - вызов метода из объекта console.
 - 'Hello world' - значение типа "String". 

```console.dir() ``` // Отображать все свойства объекта
```console.table()``` // Все свойства объекта в табличном виде

**Любое значение всегда возвращает выражение**
Примеры:
```javascript
    'abc' // 'abc'
    10 // 10
    5 + 10 // sum 5 and 10 = 15
    c = 10 
    'test' + 'test2' // 'test test2'
    a <= b || c !== d // true or false
    function(a,b) // result function 
```

