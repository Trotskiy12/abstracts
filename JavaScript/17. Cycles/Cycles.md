# Циклы

Типы циклов:
    - for
    - for ... in ...
    - while
    - do ... while ...
    - for ... of ...


# do while

```javascript
do {
    // Блок кода, выполняемый на каждой итерации
} while (Условие)


let i = 0
do {
    console.log(i)
    i++
} while (i < 5)
```

# for in

```javascript
for (key in Object) {
    // Действия с каждым свойством объекта
    // Значения свойства -- Object[key]
}
```
Пример:
```javascript
const obj = {
    x: 10, 
    y: true, 
    z: 'abc'
}

for (const key in obj) {
    console.log(key, obj[key])
}
```

# ForEach для объектов

Object.keys(obj) -- массив ключей объекта 
```javascript
const obj = { ... }

Object.keys(obj).forEach(key => {
    console.log(key, obj[key])
})
```

Object.values(obj) -- массив значений объект
```javascript
const obj = { ... }

Object.values(obj).forEach(value => {
    console.log(value)
})
```

# for in для массивов
```javascript
const arr = [true, 10, 'abc', null];

for (const key in arr) {
    console.log(arr[key]);
}
```

# for of (es6)

Iterable - любое значение по которому можно итерироваться. (строка, массив)

```javascript
for (Element of Iterable) {
    // действия с определенным элементом
}
// Цикл отработает три раза
// h e y
const str = 'hey'
for(conts letter of str) {
    console.log(letter)
}
```

for of не используется для объектов, так как объект - это неитерируемый элемент.