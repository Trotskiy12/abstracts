# Инструкции

Выражение всегда возвращает значение
Инструкция выполнет действие

Пример:
```javascript
let a;

const b = 5;

if(a > b) {
    console.log(...);
}

for(let i = 0; i++; i < 5) {
    console.log(i)
}
```

# Выражения - инструкции
```javascript
'abc'

a = a + 6;

console.log('Hey')
```