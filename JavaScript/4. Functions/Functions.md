# Функции

Функция - это блок кода, который можно выполнять многократно

```javascript
const a = 5;
const b = 5;
function sum(a, b){
    const c = a + b;
    console.log(c)
}

sum(a,b) // 10
const num1 = 100;
const num2 = 100;

sum(num1, num2) // 200; 
```

Типы функций:
1. Именованные
2. Присвоенные переменной
3. Анонимной
4. Аргуметом при вызове другой функции 
5. Методом объекта

## Объявление и вызов
```javascript
// имя(параметры)
function name(a, b) {
    // тело
    let c 
    a = a + 1
    c = a + b
    return c // результат
}

const sum = name(5,5) // 11
```


Функция возвращает undefined, если нет инструкции return.

## Передача значений по ссылке
```javascript
const personOne = {
    name: 'test',
    age: 22
}
// функция мутирует внешний объект
function increasePersonAge(person) {
    persom.age += 1
    return person
}

increasePersonAge(personOne) // передача объекта по ссылке
console.log(personOne.age) // 23
```

Внутри функции не рекомендуется мутировать внешние объекты

Как быть? Ответ: создать копию объекта
```javascript
const personOne = {
    name: 'test',
    age: 22
}

function increasePersonAge(person) {
    const updatePerson = Object.assign({}, person)
    updatePerson.age += 1
    return updatePerson
}

const updatedPersonOne = increasePersonAge(personOne)
console.log(personOne.age) // 22
console.log(updatedPersonOne.age) // 23
```

## Callback functions

Такое название, так как одна функция, вызывает внутри себя другую функцию.

```javascript
function anotherFunc() {
    // action...
}
****
function fnWithCallback(callbackFunc) {
    callbackFunc()
}

fnWithCallback(anotherFunc)
```

Пример: 

**setTimeout** внутри себя будет вызывать функцию **printMyName**

```javascript
function printMyName() {
    console.log('Daniil')
}

setTimeout(printMyName, 1000)
```

## Правила работы с функциями

1. Наименование функции исходит из задач, которые она будет выполнять
2. Одна функция должна выполнять одну задачу
3. Не рекомендуция изменять внешние, относительно функции переменные (clear function)