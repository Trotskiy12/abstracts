# Функицональные выражения

1. Объявленная функция VS Функциональное выражание
Нет имени. 
```javascript
function myFn(a,b) {
    let c
    // ...
    return c
}

myFn(5, 10)
```

```javascript
function (a, b) {
    let c
    // ...
    return c 
}
```

Какой можно сделать вывод? 
Фунциональные выражения всегда **анонимные**

|  | Объявленная функция | Функциональное выражение |
| :-------------: |:-------------:| :-------------:|
| Имеет имя | + | - |
| Автономное использование | + | - |
| Присваивать переменной | + | + |
| Использование как аргумента в вызове другой функции | + | + (чаще) |

 - Присваивание. Пример
    ```javascript
        const myFn = function (a, b) {
            // ...
        }

        myFn(5,3)
    ```
 - Фукциональное выражение в вызове другой функции
    ```javascript
        setTimeout(function () {
            console.log('test')
        }, 1000)
    ```
# Стрелочные функции
```javascript
    (a, b) => {
        //...
        return c
    }
```

Стрелочная функция - выражение - анонимная

```javascript
const myFn = (a,b) => {
    //...
}

myFn(5,3)
```

```javascript
    setTimeout(() => {
        console.log('test')
    }, 1000)
```

# Значения параметров функции по умолчанию

```javascript
function multByFunc(value, multiplier = 1) {
    return value * multiplier
}
multByFunc(10,2) // 20
multByFunc(1) // 1
```

```javascript 
const newPost = (post, addedAt = Date()) => ({
    ...post,
    addedAt,
})

const firstPost = {
    id: 1,
    author: 'Daniil'
}

newPost(firstPost)
```

