# Конкатенация строк
```javascript
'Hello ' + 'wordl'

const hello = 'hi'
const name = 'wordl'

const greeting = hello + ' ' name;
```

# Шаблонны строки
```javascript

const greeting = 'Hello`;
const name = 'Daniil';

const greeting = `${greeting} ${name}`
```

undefined + 'abc' = undefinedabc
