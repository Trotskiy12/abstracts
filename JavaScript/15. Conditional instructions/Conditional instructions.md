# Условные инструкции
Классика: 
    if
    if ... else
    switch

- if 
    ```javascript

    const person = { age: 20 }
    if (!person.name) { /// !undefined === true
        console.log('Name is undefined')
    }
    ```

- else 
    ```javascript
    let val = 10
    if(val < 5) { val += 20} 
    else { val -= 20}
    // - 10
    ```
- if else if 
    ```javascript
        if(условие 1) {
            // выполняется однократно, если условие 1 true
        } else if (условие 2) {
            // выполняется однократно, если условие 2 true
        } else {
            // выполняется однократно, если условия 1 - 2 false
        }
    ```
    Также можно реализовать через 3 инструкции if. Но, если мы хотим, что отработал только один if нам необходимо учитывать другие условия, из предыдущих if.

# Пример использование if в функциях
```javascript
const sumPositiveNumbers = (a, b) => {
    if (typeof a !== 'number' || typeof !== 'number') {
        return 'One of the arguments is not a number';
    }

    if (a <= 0 || b <= 0) {
        return 'Numbers are not positive'
    }

    return a + b;
}

```

# Switch 
Альтернатива if else if.

default - выполнение по умолчанию.
case - если выражение совпадает со случаем. (key === value), то отбработает case

```javascript
switch (key) {
    case value:
        break;

    default:
        break;
}
```