# Деструктуризация объектов

Объявление новых переменных и присваивание значений на основе значений свойств объекта.
(Порядок не важен)

```javascript
const userProfile = {
    name: 'Daniil',
    commentsQty: 23,
    hasAgree: false,
}

const { name , commentsQty } = userProfile;
const { hasAgree } = userProfile;
```

# Деструктуризация массивов

Похожим образом производится деструктуризация массивов:
Вместо {} - [], так как работаем с массивом.
(Порядок важен)

```javascript
const fruits = ['Apple', 'Banana'];
const [first, second] = fruits;

console.log(first); // Apple
console.log(second); // Banana
```

# Деструктуризация параметров функции

{ name, commentsQty } - деструктуризация входного объекта. Создать переменные и присвоить их значения.

Доступны они будут только внутри тела функции.

```javascript
const userProfile = {
    name: 'Daniil',
    commentsQty: 23,
    hasAgree: false,
}

const userInfo = ({ name, commentsQty }) => {
    if(!commentsQty) {
        return `User ${name} has no comments`;
    }
    return `User ${name} has ${commentsQty} comments`
}

userInfo(userProfile) // User Daniil has 23 comments
```