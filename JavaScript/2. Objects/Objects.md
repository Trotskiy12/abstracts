# Объекты
1. При присвоении объекта в переменную мы храним ссылку на этот объект в памяти.
```javascript
    const user = {
        name: 'Daniil',
        age: 20,
        city: 'Petrozavodsk'
    }
```

2. Порядок свойств не имеет значения.
3. Получать доступ к значениям можно через точку. (Dot notation)
4. Также можно менять значение через точку.

## Создание и удаление 

Создание новых свойств
```javascript
const someCity = {
    city: 'Petrozavodsk'
}

someCity.popular = true;
someCity.country = 'Russia';
```

## Удалаени свойтсв

```typescript
const someCity = {
    city: 'Petrozavodsk',
    popular: true
}

delete someCity.popular

console.log(someCity) // {city: 'Petrozavodsk'}
```

## Досутп к значениям

Также ещё одним способо для доступа к значению свойства - это использование квадратных скобок (bracket notation)

```javascript
const someCity = {
    cite: 'Ptz',
    country: 'Russia'
}

someCity['popular'] = false; // {city: 'Ptz', country: 'Russia', popular: true}

const countOfPeoplePropertyName = 'peopleCount'

someCity[countOfPeoplePropertyName] = 300000;

// {city: 'Ptz', country: 'Russia', popular: 'false', peopleCount: 300000}

```

## Вложенные свойства

Другими словами вложенные свойства - это объект, внутри объекта

```javascript
const someCity = {
    city: 'Ptz',
    info: {
        isPopular: false,
        country: 'Russia'
        peopleCount: 300000,
    }
}
```

Доступ будет осуществляться таким же образом, как и в случае объекта:
```javascript
// Dot notation
console.log(someCity.info.isPopular) // false

// Bracket notation
console.log(someCity.info['isPopular'])

delete someCity.info['isPopular'];
```

## Сокращенный формат записи свойств

Рассмотрим пример с использоваием переменных при создании свойств. Сначала JS проинициализирует переменные, а затем уже подставит значние в свойства объекта.
```javascript
const name = 'Daniil';
const age = 23;

const userProfile = {
    name: name,
    age: age,
    hasAppartaments: false;
}
``` 

Но такую запись можно упростить:
```javascript
const name = 'Daniil';
const age = 23;

const userProfile = {
    // лучше размещать в начале объекта
    name,
    age,
    hasAppartaments: false;
}
```

## Глобальные объекты

window - браузеры, global - node.js

Глобальный объект также содержит набор свойств и методов.

Как пример: Метод console - это свойство window/global.

## Методы объекта

Метод - это свойство объекта, значение которого - функция.

```javascript
const someCity = {
    city: 'Ptz;
    cityGreeting: function () {
        console.log('Hello')
    }
}

someCity.cityGreeting() // вызов метода
```

Можно убирать function -> cityGreeting() { ... }

Доступ к значению **свойства**: someCity.city
Вызов метода: someCity.cityGreeting()

## JSON

Javascript Object Notation - формат обмена данными.

Передача в виде строки

```json
{
    "userId": 10,
    "idTask": "CU-2010",
    "title": "Add new form component to page",
    "status": {
        "completed": false,
        "taskStatus": "in progress"
    }
}
```

Как конвертировать в JS объект? -- **JSON.parse()**
А как обратно? -- **JSON.stringify()**

