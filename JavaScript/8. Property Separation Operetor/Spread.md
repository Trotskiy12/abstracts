# Оператор spread
```javascript
const button = {
    width: 200,
    text: 'Buy'
}

const redButton = {
    ...button, 
    color: 'red
}

console.log(redButton)
```

...button - подтянет все свойства из button. Если какой-то значение уже было в объекте, то он перезапишется.

Мутирования свойств не будет, если нет вложенных объектов.