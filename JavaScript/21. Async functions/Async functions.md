# Async / await

async / await - специальный синтаксис для упрощения работы с промисами

Асинхронная функция - возвращает promise

```js
async function name(params) {
    
}

const fn = async () => {

}
```

# Await in Async functions

await - ожидаем результат другого промиса.

```js
const fn = async () => {
    await <Promise>
}

fn()
```

Пример: 
```js
const timerPromise = () => 
    new Promise((resolve, reject) =>
        setTimeout(() => resolve(), 2000))

const asyncFn = async () => {
    console.log('Timer starts')
    await timerPromise() // функиця дальше не выполняется, пока не получен результат промиса
    console.log('Timer ended')
}

asyncFn()
```

Пример: Отслеживаем выполнение промиса
```js
const timerPromise = () => 
    new Promise((resolve, reject) =>
        setTimeout(() => resolve(), 2000))

const asyncFn = async () => {
    console.log('Timer starts')
    const startTime = perfomance.now()
    await timerPromise() // ждем 
    const endTime = perfomance.end()
    console.log('Timer ended', endTime - startTime); // мс
}

asyncFn()
```

# Переход с промисов на async/await

```javascript
const getData = (url) => 
    new Promies((resolve, reject) =>
        fetch(url)
            .then(res => res.json())
            .then(json => resolve(json))
            .catch(error => reject(error))
    )
getData('https://jsonplaceholder.typicode.com/todos')
    .then(data=> console.log(data))
    .catch(error => console.log(error.message))
```
Переход на async/await
```js
const getData = async (url) => {
    const res = await fetch(url) 
    const json = await res.json()
    return json
}

const url = 'https://jsonplaceholder.typicode.com/todos'

const data = await getData(url); // нет обработки ошибки!!!
```

В коде выше нет обработки ошибок, если такая возникнет, то мы получим "непойманную" ошибку.
Как обработать ошибку в асинхронной функции?
Для этого можно использовать try/catch
```js
const getData = async (url) => {
    const res = await fetch(url) 
    const json = await res.json()
    return json
}

const url = 'https://jsonplaceholder.typicode.com/todos'

try {
    const data = await getData(url); 
    console.log(data);
} catch (error) {
    console.log(error.message);
}
```

# Резюме
1. async/await - это **синтаксическая** надстройка над промисами.
2. **await** синтаксис возможен только внутри **async** функций.
3. **async** фунцкии всегда возвращает **Promise**.
4. **async** функция ожидает результата инструкции **await** и не выполняет последущие инструкции.
5. Promise не блокирует выполнение других частей приложения.