import { convertDate } from '../../utils/convert-date';
import './ConstDate.css'
export const ConstDate = (props) => {
    return (
        <div className='cost-date'>
            <div className='cost-date__month'>{convertDate(props.date, 'month')}</div>
            <div className='cost-date__year'>{convertDate(props.date, 'year')}</div>
            <div className='cost-date__day'>{convertDate(props.date, 'day')}</div>
        </div>
    )
}