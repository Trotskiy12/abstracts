import { Card } from '../Card/Card';
import { ConstDate } from '../ConstDate/ConstDate';
import './CostItem.css';

export const CostItem = (props) => {
    return (
        <Card className='cost-item'>
            <ConstDate
                date={props.date}
            />
            <div className='cost-item__description'>
                <h2>{props.name}</h2>
                <div className='cost-item__price'>${props.amount}</div>
            </div>
        </Card>
    )
}