import './Costs.css';
import { CostItem } from '../ConstItem/ConstItem';
import { Card } from '../Card/Card';
export const Costs = (props) => {
    return (
        <Card className='costs'>
            {props.data.map(item =>
                <CostItem
                    date={item.date}
                    name={item.name}
                    amount={item.amount}
                />
            )}
        </Card>
    )
}