export const convertDate = (date, option) => {
    switch (option) {
        case 'month':
            return date.toLocaleString('ru-RU', { month: 'long' })
        case 'year':
            return date.getFullYear();
        case 'day':
            return date.toLocaleString('ru-RU', { day: '2-digit' })
        default:
            return date
    }
}
