import { CostItem } from "./components/ConstItem/ConstItem";
import { Costs } from "./components/Costs/Costs";

function App() {

  const costs = [
    {
      date: new Date(),
      name: 'Холодильник',
      amount: 9999
    },
    {
      date: new Date(2021, 3, 14),
      name: 'Пылесос',
      amount: 9999
    },
    {
      date: new Date(2022, 7, 20),
      name: 'Пылесос',
      amount: 9999
    }
  ]

  return (
    <div>
      <Costs data={costs} />
    </div>
  );
}

export default App;
