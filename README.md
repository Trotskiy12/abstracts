# Репотизиторий для конспеков

_Ученье – свет, а не ученье – это тьма!_

## На какие темы будут вестись конспекты?

Темы для конспектов связаны с Frontend-разработкой.

### Список конспектов

1. JS
2. TS
3. React

#### Прогресс
##### TypeScript
1. 03.09.22 -- Создан репозиторий на GitLab
2. 03.09.22
   - Проблемы JS и почему нужен TS.
   - Основные типы: number, string, boolean.
   - Объекты, массивы.
   - Приведение к типу.
   - Типизации фукнции.
3. 03.09.22 (2)
   - Что такое Tuples в TS.
   - Свойство ReadOnly
   - Что такое enum и его типы: Константный enum, Гетерогенный enum, Числовой enum
4. 04.09.22
   - Union Types
   - Narrowing
   - В процессе Literal Types
5. 06.09.22
   - Закончен Literal Types
   - Type Alias
6. 07.09.22
   - Interfaces:
     - Написание интерфейсов
     - Расширешие с помощью extends
     - Преимещуства использования type or interface
     - Индексное свойство
     - Обзорно рассмотрен Record
   - Option:
     - Опциональные параметры
     - Использование опциональных параметров в функции и в объекте
     - Optional Chaning
     - Nullish Coalescing
     - Использование "!" вместо "?"
   - Задача на типизацию ответа от сервера (см. task-3.ts)
7. 08.09.22
   - Void
   - Never
   - Начал Never
8. 12.09.22
   - Never
   - Null
   - Приведение типов
9. 13.09.22
   - Type Guard
   - Create Class and strictPropertyInitialization
10. 14.09.22
   - Methods
   - Overload methods
   - Getter & Setter
   - Implements
11. 15.09.22
   - Extends
   - Features extends
   - Composition VS Extends
   - Property Visibility
   - Static property
   - Work with this
12. 20.09.22
   - Work with this
   - Typing this
   - Abstract classes
13. 21.09.22 
   - Compilation output
   - Language and environment
   - Modules
   - Strict mode
14. 22.09.22
   - Code checks
   - Embedded generic
   - Funtions with Generic
   - Using in Types
   - Generic restriction
   - Generic classes
   - Mixins

##### JavaScript
1. 05.10.22 
   Освежил знания:
   - Base
   - Variables
   - Objects
   - Object mutation
   - Functions
2. 10.10.22
   Освежил знания:
   - Scropes
   - Operators
   - Spread
   - Concatenation
3. 11.10.22
   - Func exp
   - error processing
   - instruction
   - arrays
4. 12.10.22
   - Desructuring
   - Conditional instructions
   - Ternarny operator
   - Cycles
   - Modules
5. 13.10.22
   - Modules 
   - Classes
6. 15-16.10.22
   - Promise
   - Async func
   - Course 1 complete
##### Полезные ссылки

1. https://htmlacademy.ru/blog/articles/markdown -- Markdown за 5 минут.
2. https://www.atlassian.com/ru/git/tutorials/saving-changes/gitignore -- .gitignore.
3. https://www.typescriptlang.org/docs/handbook/2/narrowing.html -- Narrowing TypeScript

#### TODO по репозиторию

1. Добавить и настроить pretty-quick.
2. ~~Разбить раздел Advanced Types на параграфы. Каждый параграф в отдельном файле.~~
3. Проверить на грамматические ошибки.
