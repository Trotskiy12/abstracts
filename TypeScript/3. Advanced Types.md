# Продвинутые типы

**Interface & Types - упрощение и переиспользование типизации**

#### Union Types

Union - возможность сказать, что переменная может принимать тот или иной тип, взависимости от ситуации

```typescript
// id: string | number -- Union Type -> id либо строка, либо число
function logId(id: string | number | boolean) {
  console.log(id.toLowerCase()); // Это не будет работать, так как у number и boolean нет такого метода
}
```

Сужение типов. (Narrowing)
Позволяет сделать runTime проверку и сделать ограничение на тип в рамках TS.
Позволяет перейти от UnionType до конкретного типа, при определнных условиях

```typescript
function logId(id: string | number | boolean) {
  //Проверка на тип позволяет нам сузить UnionType
  if (typeof id === 'string') {
    return id.toLowerCase();
  } else {
    return id; // id : number | boolean
  }
}
```

```typescript
// Narrowing для массива (также работает и с объектам и классами и т.д.)
function logError(err: string | string[]) {
  if (Array.isArray(err)) {
    // err: string[]
  } else {
    // err: string
  }
}

// Narrowing Object
// Как проверить какой объект?
function logObject(obj: { a: number } | { b: number }) {
  // используем оператор in
  if ('a' in obj) {
    obj.a;
  } else {
    obj.b;
  }
}
// Narrowing для пересекающихся типов
function logMultiIds(a: string | number, b: number | boolean) {
  // В случае пересечения типов
  if (a === b) {
    // a и b строки
  } else {
    // остальные случаи
  }
}
```

#### Literal Types

Литеральные типы обявляются следующим образом:

```typescript
function fecthWithAuth(url: string, method: 'post' | 'get' | 'delete') {}
// При вызове fecthWithAuth() -- первый аргумент любая строка, method только post, get, delete
```

В таком виде тип является также литеральным, но в большинстве случаев он бесполезен.

```typescript
const a = 'sds'; // type a: sds
```

Использование Union & Literal Types, позволяет добавить динамики.

Аналогично для возвращаемого значения

```typescript
function fecthWithAuth(url: string, method: 'post' | 'get' | 'delete'): 1 | -1 {
  ...
  return 1
  ...
  return -1
}
```

Нюанс, который стоит учитывать при работы с Literal Types

```typescript
function fecthWithAuth(url: string, method: 'post' | 'get' | 'delete') {}

let method = 'post'; // method type is string!
/*
  будет ошибка на method, так как идёт проверка именно на тип, 
  а у функции тип один из 'post' | 'get' | 'delete'
*/
fectWithAuth('s', method);
```

Исправить этот момент, можно двумя способами:

1. Поменять **let** на **const**:

```typescript
const method = 'post'; // method type is 'post'
```

2. Либо же можно использовать cast к типу

```typescript
fectWithAuth('s', method as 'post');
```

Стоит отметить, что данный метод стоит использовать, когда мы явно понимаем, что в перменной будет только то значение, к которому мы кастомим. Не стоит исползовать as, как фикс

#### Intersection Types

#### Type Alias

Позволяет более эффективно записывать типы.

```typescript
// Хороши исползовать Type Alias
type httpMethod = 'post' | 'get' | 'delete' // существует только в TS
// Медот может вызываться где угодно и сколько угодно раз
function fecthWithAuth(url: string, method: httpMethod): 1 | -1 {
  ...
  return 1
  ...
  return -1
}
```

Рассмотрим типизацию объектов тем же способом:

```typescript
let user: {
  name: string;
  age: number;
  skills: string[];
} = {
  name: 'test',
  age: 22,
  skills: ['react', 'typescript'],
};
```

Вынесем типизацию в Type Alias:

```typescript
type User = {
  name: string;
  age: number;
  skills: string[];
};

let user: User = {
  name: 'test',
  age: 22,
  skills: ['react', 'typescript'],
};
```

1. Имеем один единый type
2. Можем манипулировать функциями в разных частях проекта

Также есть возможность писать не один type, а intersection **(работает как &&, но обозначается &)**:

```typescript
type User = {
  name: string;
  age: number;
  skills: string[];
};

type Role = {
  id: number;
  name: string;
};
// Объединение User and Role
type UserWithRole = User & Role;

// Необходимо удовлетворять обоим типам
// Отсуствие поля у объекта -- ошибка
// Если есть пересекающие поле -- то его нужно задавать один раз!
let user: UserWithRole = {
  name: 'test',
  age: 22,
  skills: ['react', 'typescript'],
  id: 1,
};
```

Если у двух типов есть поле с одинаковым названием, то & "удалит" это поле и будет не понятно, для данного **user** это его **имя** или это название его **роли**?

В таком случае необходимо сделать декомпозицию:

```typescript
...
type UserWithRole = {
    user: User,
    role: Role,
  }

const newUser: UserWithRole = {
    user: {
        name: 'Daniil',
        age: 22,
        skills: ['react', 'typescript']
    },
    role:{
        id: 1,
        name: 'Admin'
    }
};

```

#### Interfaces

Ключевое слово перед названием **interface**:

```typescript
interface User {
  name: string;
  age: number;
  skills: string[];
}

const user: User = {
  name: 'Daniil',
  age: 22,
  skills: ['react', 'typescript'],
};
```

Чтобы расширить interface необходимо использоать **extends**:

```typescript
interface User {
  name: string;
  age: number;
  skills: string[];
}

// User1WithRole будет иметь все свойства что и User + roleId
interface UserWithRole extends User {
  roleId: number;
}

const user1: UserWithRole = {
  name: 'Daniil',
  age: 22,
  skills: ['react', 'typescript'],
  roleId: 1,
};
```

Ещё одно использование (схожее с extention в type):

```typescript
interface User {
  name: string;
  age: number;
  skills: string[];
}

interface Role {
  roleId: number;
}
// UserWithRole будет иметь все свойства из User и Role
interface UserWithRole extends User, Role {
  // Такой интерфес может иметь и свои собсвтенные свойства
  createdAt: Date;
}

const user1: UserWithRole = {
  name: 'Daniil',
  age: 22,
  skills: ['react', 'typescript'],
  roleId: 1,
  createdAt: new Date(),
};
```

Описание функций:

```typescript
interface User {
  name: string;
  age: number;
  skills: string[];
  // Функция, которая принимат id (number) return string[]
  // Такое описание идентично описанию метода в классе
  log: (id: number) => string[];
}

interface Role {
  roleId: number;
}

interface UserWithRole extends User, Role {
  createdAt: Date;
}

const userWithRole: UserWithRole = {
  name: 'Daniil',
  age: 22,
  skills: ['react', 'typescript'],
  roleId: 1,
  createdAt: new Date(),
  // теперь в объекте должно быть описание функции log
  log(id) {
    return [''];
  },
};
```

Индексное свойство (частое использование -- словари):

```typescript
/*
    Какое поведение необходимо:
    {
        1: user1,
        2: user2
    }
*/

interface UserDictionary {
  // Эта запись означает, что у интерфейса может быть неограниченное число свойств
  // Где ключом является число, а значение User
  // Таким образом получаем описание словаря
  [index: number]: User;
}

// Аналогичная запись и для type

type UserDictionary2 = {
  // Эта запись означает, что у интерфейса может быть неограниченное число свойств
  // Где ключом является число, а значение User
  // Таким образом получаем описание словаря
  [index: number]: User;
};

/*
    Record:
    Создает тип объекта, ключами свойств которого являются Keys, 
    а значениями свойств — Type. 
    Эту утилиту можно использовать для отображения свойств одного типа на другой тип.
*/
type ud = Record<number, User>;

// Record example:
interface CatInfo {
  age: number;
  breed: string;
}

type CatName = 'miffy' | 'boris' | 'mordred';

const cats: Record<CatName, CatInfo> = {
  miffy: { age: 10, breed: 'Persian' },
  boris: { age: 5, breed: 'Maine Coon' },
  mordred: { age: 16, breed: 'British Shorthair' },
};

cats.boris; // const cats: Record<CatName, CatInfo>
```

### Types or Interface?

В чём отличие между **type** и **interface**:

1. Небольшое преимущесвтво Interface:

```typescript
// Есть интерфейс User
interface User {
  name: string;
}
// В другом месте программы захотели расширить интерфейс

interface User {
  age: number;
}
// В таком случаи интерфейс User расшириться -- происходит merge
const user2: User = {
  name: 'Daniil',
  age: 22,
};
```

Когда это полезно, а когда во вред?

1.1 Полезно, если мы используем библиотеку и у нас нет возможности exntends тот тип.
Тогда можно этот тип доопределить и появится возможность использовать этот тип с необходимыми свойствами.

1.2 Во вред, если мы пишем свой интерфейс, который доопределяется в другом месте.
При ревью, будут возникать вопросы, где доопределение?
Таким образом можно ввести в заблуждение
Просто, понятно и лаконично дополнить тип, без доопределения

2. Преимущество Type:

```typescript
type ID = string | number;
// С interface так не получится
// Только указать внутри интерфейса свойство
interface IDI {
  ID: string | number;
}
```

- Если нам необходимо сделать Union/Intersection с приминитивными типами, то только через **type**

- Если работа с объектами, то лучше использовать interface

### Option

Допустим у нас есть интерфейс **User** с свойствами **login** и **password**, которое является необязательным (опциональным).
Чтобы сделать свойство опциональным, нам обходимо добавить знак '**?**'

```typescript
interface User {
  login: string;
  password?: string;
}

// Так как свойство password у нас опциональное, нам необязательно указывать его при создании объекта типа User
const user: User = {
  login: 'mail@email.com',
};
```

Стоит отметить, что: запись password?: string не эквивалентна записи password: string | undefined:

```typescript
interface User {
  login: string;
  password: string | undefined;
}
// Будет ошибка, так как нет свойства password
const user: User = {
  login: 'mail@email.com',
};
```

Использование опциональных типов в функциях:

```typescript
function multiplty(first: number, second?: number): number {
  return first * (second ? second : first);
}
```

В рамках объекта:

```typescript
interface UserPro {
  login: string;
  password?: {
    type: 'primary' | 'secondary';
  };
}
// Optional Chaning -- можем обратиться к свойству объекта, которого у нас может и не быть
// если нет пароля -- получим undefined
function testPass(user: UserPro) {
  const type = user?.password?.type; // <=> user.password ? user.password.type : undefined;
}

// Использование "!" -- мы гарантируем, что поле будет undefined
function testPass2(user: UserPro) {
  const type = user?.password!.type;
}
```

Nullish Coalescing:

```typescript
function test(param?: string) {
  // если у нас param null or undefined то возращаем пустую строку
  const value = param ?? '';
}
```

#### Void

Это тип существует только в TS.

Void обозначает, что функция ничего не возвращает.

```typescript
// Функция будет возвращать void
function LogId(id: string | number) {
  console.log(id);
}
// a будет иметь тиа -- void
const a1 = LogId(1);

// Если мы из функции что-то вернули, в какой-либо ветке
// То возвращаемое значение будет Union типа: number | undefined
// В том случае, если не возвращаем, то будет void
function multiply(f: number, s?: number) {
  if (!s) {
    return f * f;
  }
}

// в случае
type undefinedFunc = () => undefined;
// Определение функции будет некорректно, так как необходимо вернуть undefined
const unF: undefinedFunc = () => {};
```

Кейс для Void:

```typescript
const arr = ['react', 'js', 'nestjs'];

const userWithSkills = {
  softskills: [''],
};
// forEach в результате ожидает void
// Array.push -- возвращает длину нового массива
// благодаря void мы получаем совместимость использования forEach + push
// void игнорирует возврат
skills.forEach((skill) => userWithSkills.softskills.push(skill));
```

#### Unknown

Тип Unknown физически означает, что мы не знаем, что у нас лежит в переменной.
Используем, когда заранее не знаем, какой у нас будет тип, но точно не any

- Почему **Unknown** Не **Any**

```typescript
let input: unknown;

input = 3; // Valid
input = ['sdfg', 'wsefrgb']; // Valid

const res: string = input; // ошибка
```

Отличия:

1. Мы не можем положить unknown тип в любую другую переменную, передать в функцию,
   пока мы не сделаем приведение к типу, либо не определим, что за тип
   Можем класть unknown либо в any, либо в unknown

Сужение Unknown

```typescript
function run(i: unknown) {
  // сужаем тип
  if (typeof i === 'number') {
    i++;
    // в else всё равно unknown
  } else {
    i; // type unknown
  }
}

run(input);
```

Частый кейс для unknown: try-catch

```typescript
async function getData() {
  try {
    await fetch('');
    // до 4.4 ошибка была type: any
  } catch (error) {
    // Проверим, что error является ошибкой
    if (error instanceof Error) {
      console.log(error.message);
    }
  }
}

async function getDataForce() {
  try {
    await fetch('');
    // до 4.4 ошибка была type: any
  } catch (error) {
    // Явное приведение к типу может выдасть ошибку, например, если error будет string
    const e = error as Error;
    // Ошибка будет в логировании
    console.log(e.message);
  }
}
```

Unknown + Другие типы:

- Union Unknown с абсолютно любым типом будет unknown

```typescript
type U1 = unknown | null; // U1 type -- unknown
```

- Intersection Unknown с любым типом будет этот тип

```typescript
type U1 = unknown & number; // U1 type -- number, потому что при intersection берем поле узкий тип.
```

#### Never

Never обозначает, что "такого" никогда не произойдёт
Позволяет более эффективно писать код, сделать код более безопасным

```typescript
// TS говорит, что функция вернёт void, но это не совсем так
// Она никогда не возвращает
function generateError(message: string): never {
  throw new Error(message);
}

function dumpError(): never {
  while (true) {}
}

// Аналогично для рекурсии
// По умолчанию TS поставит any, но мы никогда не вернёмся
// Поэтому логичнее поставить never
function rec(): never {
  return rec();
}

// Никогда ничего не сможем присвоить
// В этом заключается большое отличие never от void
const a: never = null;

// Возвомжное присвоение
const a: void = undefined;

type paymentAction = 'refund' | 'checkout' | 'reject';

// При добавлении нового action получим ошибку в runTime!
// 1.
function processAction(action: paymentAction) {
  switch (action) {
    case 'refund':
      // ...
      break;
    case 'checkout':
      // ...
      break;
    case 'reject':
      // ...
      break;
    default:
      // _ -- ts не будет ругаться на неиспользуемую переменную
      // Но при добавлении нового action -- будет ошибка
      // Обеспечение реальной ошибки
      const _: never = action;
      throw new Error('Action not found');
  }
}

// 2. Исчерпывающая проверка
function isString(x: string | number): boolean {
  // Проверка на строку
  if (typeof x === 'string') {
    return true;
    // Если оставим только else -- все будет в порядке
    // Но добавив проверку на число -- получим ошибку
  } else if (typeof x === 'number') {
    return false;
  }
  // Так как есть return undefined в непроверенных случаях
  // Эта функция возвращает never -- никогда сюда не попадём
  generateError('sds');
}
```

#### Null

```typescript
const n: null = null;
// не можем присвоить null undefined
const n1: null = undefined;
// Не можем присвоить number null
// Хотя в JS мы так можем сделать

// Режим strictNullChecks -- выключен
// Ошибок не будет
// При дальнейшем использовании -- проблем не будет
// Так как в js мы так можем делать
const n2: number = null;
const n3: string = null;
const n4: boolean = null;
const n5: undefined = null;

// Но стоит рассмотреть и другие моменты:

interface User {
  name: string;
}

function getUser() {
  if (Math.random() > 0.5) {
    return;
  } else {
    return {
      name: 'Test',
    } as User;
  }
}

// Предполагается, что getUser всегда вернёт пользователя.
// Но это не так! У нас есть условие
// Если strictNullChecks: true -- то будет ошибка,
// так как user будет иметь union type -- User | undefined
const user = getUser();
const nameNew = user?.name;

// Вывод, что нужно включать эту опцию, чтобы исбежать некорректной работы
```

- Различие между null и undefined

1. Undefined - означает, что мы **не задали** этот объект.
   Можем получить, если обратимся к свойству объекта, который не существует.
   Использовать, если есть возможность, что этого объекта не существует

2. Null - это явно **заданный** неопределённый объект.
   Не можем получить
   Использовать, если мы осозанно хотим вернуть, что этого объекта нет.

#### Приведение типов

- Простые типы:

```typescript
let a_1: number = 5;
let a_2: string = a.toString();
// Стоит с отсрожностью относиться к конструктору типов
// e будет иметь тип String -- интерфейс конструктора типов для строки
let e = new String(a).valueOf(); // valueOf() -- будет строкой
let f = new Boolean(a).valueOf();

let c = 'sdfs';
let d: number = parseInt(c); // лучше, чем +с
```

- Объектные типы:

  1. Указать тип после название переменной: **const user: User = ...**
  2. Cast к интерфейсу: **const user = {...} as User**
  3. (Not Recommend): **const user = <User> {...}**. Такая запись буде невалидна для React, потому что в JSX такая запись зарезервирона под компонент.

  -- Преобразование одного объекта к другому объекту:
  Способ не рекомендуется к использованию

  ```typescript
  interface Admin {
    name: string;
    role: number;
  }
  // Минус такого подхода: сохранили свойства email и login у Admin'a
  const admin_1: Admin = {
    ...user23,
    role: 1,
    // Сейчас у admin только свойства name и role, но в JS у него уже будет больше свойств
  };
  ```

  Правильный способ - использовать фукнцию maping'a:

  ```typescript
  function userToAdmin(user: User21): Admin {
    return {
      name: user.name,
      role: 1,
    };
  }
  ```
