const data = [
	{ id: 2, name: 'Петя' },
	{ id: 1, name: 'Вася' },
	{ id: 3, name: 'Надя' },
];
// Интерфейс, который мы будем extends к Genrec'y
interface ID {
    id: number;
}
// Ограничим тип T нашем interface
// data - это у нас массив, где id обязательно number
// type - опциальное значение, по умолчанию acs
// возвращаемое значение массив, где id: number
function sort<T extends ID>(data: T[], type: 'asc' | 'desc' = 'asc'): T[] {
    return data.sort((a, b) => {
        switch (type) {
            case 'asc':
                return a.id - b.id;
            case 'desc':
                return b.id - a.id;
        }
    })
}

console.log(sort(data, 'desc'))
console.log(sort(data))
