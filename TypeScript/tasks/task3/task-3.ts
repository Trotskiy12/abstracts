interface IPayment {
    sum: number;
    from: number;
    to: number;
}

enum PaymentStatus {
    Success = 'success',
    Failed = 'failed'
}

interface IPaymentRequest extends IPayment {}
// Так как поля sum, from, to повторяются 
// Лучше унаследоваться от IPayment
interface IPaymentDataSuccess extends IPayment {
    databaseId: number;
	// sum: number;
	// from: number;
	// to: number;
}

interface IPaymentDataFailed {
    errorMessage: string;
    errorCode: number;
}

/* interface IResponse {
    status: PaymentStatus;
    Не лучший вариант, хоть и описывает правильно
    Может быть случай, что status = 'success', a data -- IPaymentDataFailed 
    data: IPaymentDataSuccess | IPaymentDataFailed;
}
*/

/*
Улучшаем.
Вместо того, чтобы делать Union на уровне данных
Сделаем его на уровень выше
Итог: Более полное описание. Отсутсиве перекрестных вариантов
*/
interface IResponseSuccess {
    status: PaymentStatus.Success;
    data: IPaymentDataSuccess;
}

interface IResponseFailed {
    status: PaymentStatus.Failed;
    data: IPaymentDataFailed;
}

// function get(): IPaymentDataSuccess | IPaymentDataFailed {

// }