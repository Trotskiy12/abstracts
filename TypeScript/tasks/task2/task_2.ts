// Количество status ограничено тремя зачениями -- используем enum
enum Status {
    Published = "published",
    Draft = "draft",
    Deleted = "deleted"
}

type faqsData = {
    question: string;
    answer: string;
    tags: string[];
    likes: number;
    status: Status;
}
// Возвращать функция должна Promise так как она асинхронная
// type должен быть как в Promise так и определен у даты. Дублирование не позволит быть коду более читаемым, поэтому используем faqsData
async function getFaqs(req: {
    topicId: number;
    status?: Status;
}): Promise<faqsData[]> {
	const res = await fetch('/faqs', {
		method: 'POST',
		body: JSON.stringify(req)
	});
	const data: faqsData[] = await res.json();
	return data;
}