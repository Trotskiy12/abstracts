function toStringGeneric(data) {
    if (!data)
        return undefined;
    if (Array.isArray(data))
        return data.toString();
    switch (typeof data) {
        case 'string':
            return data;
        case 'number':
        case 'boolean':
        case 'symbol':
        case 'bigint':
        case 'function':
            return data.toString();
        case 'object':
            return JSON.stringify(data);
        default:
            return undefined;
    }
}
console.log(toStringGeneric(3));
console.log(toStringGeneric(true));
console.log(toStringGeneric(['a', 'b']));
console.log(toStringGeneric({ name: 'test' }));
