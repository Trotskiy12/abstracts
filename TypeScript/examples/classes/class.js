var User = /** @class */ (function () {
    // Сделаем name необязательным, тем самым покрываем остальные конструкторы
    function User(ageOrName, age) {
        if (typeof ageOrName === 'string') {
            this.name = ageOrName;
        }
        else if (typeof ageOrName === 'number') {
            this.age = ageOrName;
        }
        if (typeof age === 'number') {
            this.age = age;
        }
    }
    return User;
}());
var user = new User('Danill');
// Без передачи имени, но сохранить функционал
var user2 = new User();
// Присвоить только возраст
var user3 = new User(33);
// И возраст и Имя
var user4 = new User('Daniil', 22);
