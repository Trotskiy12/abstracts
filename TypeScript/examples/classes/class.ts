class User {
    name: string;
    age: number;

    // overload 
    constructor();
    constructor(name: string);
    constructor(age: number);
    constructor(name: string, age: number);
    // Сделаем name необязательным, тем самым покрываем остальные конструкторы
    constructor(ageOrName?: string | number, age?: number) {
        if(typeof ageOrName === 'string') {
            this.name = ageOrName;
        } else if (typeof ageOrName === 'number'){
            this.age = ageOrName;
        }
        if (typeof age === 'number'){
            this.age = age;
        }
    }
}

const user = new User('Danill');
// Без передачи имени, но сохранить функционал
const user2 = new User();
// Присвоить только возраст
const user3 = new User(33);
// И возраст и Имя
const user4 = new User('Daniil', 22)
