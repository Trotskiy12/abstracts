var StatusCode;
(function (StatusCode) {
    StatusCode[StatusCode["SUCCESS"] = 1] = "SUCCESS";
    StatusCode[StatusCode["IN_PROCESS"] = 3] = "IN_PROCESS";
    StatusCode[StatusCode["FAILED"] = 4] = "FAILED";
})(StatusCode || (StatusCode = {}));
function logObject(obj) {
    // используем оператор in
    if ('a' in obj) {
        obj.a;
    }
    else {
        obj.b;
    }
}
var a = 1;
console.log(typeof a);
