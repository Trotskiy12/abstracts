function multiplty(first: number, second?: number): number{
    return first * (second ? second : first);
}

interface UserPro {
    login: string,
    password?: {
        type: 'primary' | 'secondary',
    }
}
// Optional Chaning -- можем обратиться к свойству объекта, которого у нас может и не быть
// если нет пароля -- получим undefined
function testPass(user: UserPro) {
    const type = user?.password?.type
}

// Использование "!" -- мы гарантируем, что поле будет undefined
function testPass2(user: UserPro) {
    const type = user?.password!.type
}

// Nullish Coalescing
function test(param?: string) {
    // если у нас param null or undefined то возращаем пустую строку
    const value = param ?? ''
}