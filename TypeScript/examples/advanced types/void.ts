// Функция будет возвращать void
function LogId(id: string | number) {
    console.log(id)
}
// a будет иметь тиа -- void
const a1 = LogId(1);

// Если мы из функции что-то вернули, в какой-либо ветке
// То возвращаемое значение будет Union типа: number | undefined
// В том случае, если не возвращаем, то будет void
function multiply(f: number, s?: number){
    if(!s) { return f * f}
}

// Type Для обозначения функции
type voidFunc = () => void;

const f1: voidFunc = () => {}
// Это будет работать, так как voidFunc будет говорить:
// Вернуть можно всё, что угодно, но return будет игнорироваться
const f2: voidFunc = () => {
    return true;
}

// b type -- void
const b = f2();

// в случае 
type undefinedFunc = () => undefined;
// Определение функции будет некорректно, так как необходимо вернуть undefined
const unF: undefinedFunc = () => {}

const arr = ['react', 'js', 'nestjs']

const userWithSkills = {
    softskills: [''],
}
// forEach в результате ожидает void
// Array.push -- возвращает длину нового массива
// благодаря void мы получаем совместимость использования forEach + push
// void игнорирует возврат
skills.forEach((skill) => userWithSkills.softskills.push(skill))

