// Tuples
// При попытке добавить что-то отличное от строки -- будет ошибка
// Необходимо расширить тип для переменной softs

import { string } from "yup"

// Самое простое решение -- добавить any, но проблема, что использовать можно всё что угодно
const Skills: [number, string] = [1, 'Dev']
const id = Skills[0]
const skillName = Skills[1]
// const q = Skills[3] -- не можем обраться к элементу вне описания

// Методы работают без ограничения так как выполняются в runTime
Skills.push('sdsdhj')
// Даже после push мы не можем обратиться ко второму элементу массива
Skills.pop()

// Также работает деструктаризация
const [Id, SkillName] = Skills;

// ...boolean[] -- означает, что может быть любое количество boolean переменных
const arr: [number, string, ...boolean[]] = [1, 'Dev', true, false, true] 

// readonly 
const testArr: readonly [...number[]] = [1,2,3,4,5,6]

const readonlyArr: readonly string[] = ['1', '2']
// readonlyArr.push()