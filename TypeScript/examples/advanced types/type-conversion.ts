let a_1: number = 5;
let a_2: string = a.toString();
// Стоит с отсрожностью относиться к конструктору типов
// e будет иметь тип String -- интерфейс конструктора типов для строки
let e = new String(a).valueOf(); // valueOf() -- будет строкой
let f = new Boolean(a).valueOf();

let c = 'sdfs';
let d: number = parseInt(c); // лучше, чем +с

interface User21 {
    name: string;
    email: string;
    login: string;
}

const user23 = {
    name: 'Test',
    email: 'test@email.com',
    login: 'test123'
} as User21

interface Admin {
    name: string;
    role: number;
} 
// Минус такого подхода: сохранили свойства email и login у Admin'a
// const admin_1: Admin = user;
// Сейчас у admin только свойства name и role, но в JS у него уже будет больше свойств
// const admin_1: Admin = user23;
// 

function userToAdmin(user: User21): Admin {
    return {
        name: user.name,
        role: 1
    }
}