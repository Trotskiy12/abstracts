interface User {
    name: string;
    age: number;
    skills: string[];
    // Функция, которая принимат id (number) return string[]
    log: (id: number) => string[]
};

interface Role {
    roleId: number
}
// UserWithRole будет иметь все свойства из User и Role
interface UserWithRole extends User, Role {
    // Такой интерфес может иметь и свои собсвтенные свойства
    createdAt: Date
}

const userWithRole: UserWithRole = {
    name: 'Daniil',
    age: 22,
    skills: ['react', 'typescript'],
    roleId: 1,
    createdAt: new Date(),
    log(id){
        return ['']
    }
};

/*
    Какое поведение необходимо:
    {
        1: user1,
        2: user2
    }
*/

interface UserDictionary {
    // Эта запись означает, что у интерфейса может быть неограниченное число свойств
    // Где ключом является число, а значение User
    // Таким образом получаем описание словаря
    [index: number]: User
}

// Аналогичная запись и для type

type UserDictionary2 = {
    // Эта запись означает, что у интерфейса может быть неограниченное число свойств
    // Где ключом является число, а значение User
    // Таким образом получаем описание словаря
    [index: number]: User
}

/*
    Record:
    Создает тип объекта, ключами свойств которого являются Keys, 
    а значениями свойств — Type. 
    Эту утилиту можно использовать для отображения свойств одного типа на другой тип.
*/
type ud = Record<number, User>

// Record example:
interface CatInfo {
    age: number;
    breed: string;
  }
   
  type CatName = "miffy" | "boris" | "mordred";
   
  const cats: Record<CatName, CatInfo> = {
    miffy: { age: 10, breed: "Persian" },
    boris: { age: 5, breed: "Maine Coon" },
    mordred: { age: 16, breed: "British Shorthair" },
  };
   
  cats.boris;

// Есть интерфейс User1
interface User1 {
    name: string;
}
// В другом месте программы захотели расширить интерфейс

interface User1 {
    age: number;
}
// В таком случаи интерфейс User1 расшириться -- происходит merge
const user2: User1 = {
    name: 'Daniil',
    age: 22
}