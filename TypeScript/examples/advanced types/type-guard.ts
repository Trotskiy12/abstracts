interface User21 {
    name: string;
    email: string;
    login: string;
}

const user_12 = {
    name: 'Test',
    email: 'test@email.com',
    login: 'test123'
} as User21

interface Admin {
    name: string;
    role: number;
} 

function logId(id: string | number) {
    if (isString(id)) {
        console.log(id);
    } else {
        console.log(id)
    }

    // id // type is string | number -- flow types
}

// Пример Type Guard
// x is string -- показывает, что мы проверили -- х - это строка
// return boolean
function isString(x: string | number): x is string {
    return typeof x === 'string';
}

// Пример Type Guard
// Входной параметр может иметь разный интерфейс
// По своей сути user is Admin -- это boolean, но если поставить boolean - мы не будем ограничивать наши типы
function isAdmin(user: User21 | Admin): user is Admin {
    return 'role' in user;
}
// Альтернативная запись
// Проверка через cast типов
// Помогает в случае вложенности свойств
function isAdminAlt(user: User21 | Admin): user is Admin {
    return (user as Admin)?.role !== undefined;
}

function setRoleZero(user: User21 | Admin) {
    if (isAdmin(user)) {
        user.role = 0;
    } else {
        throw new Error('User is not Admin');
    }
}
