// type User = {
//     name: string;
//     age: number;
//     skills: string[];
//   };
  
//   type Role = {
//     id: number;
//     name: string;
//   };

// type UserWithRole = {
//     user: User,
//     role: Role,
//   }
  
// const newUser: UserWithRole = {
//     user: {
//         name: 'Daniil',
//         age: 22,
//         skills: ['react', 'typescript']
//     },
//     role:{
//         id: 1,
//         name: 'Admin'
//     }
// };
  