const n: null = null;
// не можем присвоить null undefined
const n1: null = undefined;
// Не можем присвоить number null
// Хотя в JS мы так можем сделать

// Режим strictNullChecks -- выключен
// Ошибок не будет
// При дальнейшем использовании -- проблем не будет
// Так как в js мы так можем делать
const n2: number = null;
const n3: string = null;
const n4: boolean = null;
const n5: undefined = null;

// Но стоит рассмотреть и другие моменты:

interface User {
    name: string;
}

function getUser() {
    if (Math.random() > 0.5 ){
        // Осознанное отсуствие
        return null;
    } else {
        return {
            name : 'Test'
        } as User
    }
}

// Предполагается, что getUser всегда вернёт пользователя.
// Но это не так! У нас есть условие
// Если strictNullChecks: true -- то будет ошибка,
// так как user будет иметь union type -- User | undefined
const userNew = getUser();
// Добавим проверку
if (userNew) {
    const nameNew = userNew.name;
}

// Вывод, что нужно включать эту опцию, чтобы исбежать некорректной работы