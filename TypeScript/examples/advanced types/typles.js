// Tuples
// При попытке добавить что-то отличное от строки -- будет ошибка
// Необходимо расширить тип для переменной softs
// Самое простое решение -- добавить any, но проблема, что использовать можно всё что угодно
var Skills = [1, 'Dev'];
var id = Skills[0];
var skillName = Skills[1];
// const q = Skills[3] -- не можем обраться к элементу вне описания
// Методы работают без ограничения так как выполняются в runTime
Skills.push('sdsdhj');
// Даже после push мы не можем обратиться ко второму элементу массива
Skills.pop();
// Также работает деструктаризация
var Id = Skills[0], SkillName = Skills[1];
// ...boolean[] -- означает, что может быть любое количество boolean переменных
var arr = [1, 'Dev', true, false, true];
// readonly 
var testArr = [1, 2, 3, 4, 5, 6];
console.log('old', testArr);
testArr[0] = 10;
console.log('new', testArr);
