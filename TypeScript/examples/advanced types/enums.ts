enum StatusCode {
    SUCCESS = 1,
    IN_PROCESS = 3,
    FAILED = 4
}

function logObject(obj: { a: number } | { b: number }) {
    // используем оператор in
    if('a' in obj){
        obj.a
    } else {
        obj.b
    }
}

const a: 1 = 1
console.log(typeof a)