let input: unknown;

function run(i: unknown) {
    // сужаем тип
    if(typeof i === 'number'){
        i++;
    // в else всё равно unknown
    } else {
        i  // type unknown
    }
}

run(input)

async function getData(){
    try {
        await fetch('')
    // до 4.4 ошибка была type: any
    } catch(error) {
        // Проверим, что error является ошибкой
        // Проверка более явная
        if (error instanceof Error) {
            console.log(error.message)
        }
    }
}

async function getDataForce(){
    try {
        await fetch('')
    // до 4.4 ошибка была type: any
    } catch(error) {
        // Явное приведение к типу может выдасть ошибку, например, если error будет string
        const e = error as Error;
        // Ошибка будет в логировании
        console.log(e.message)
    }
}

type U1 = unknown | null;