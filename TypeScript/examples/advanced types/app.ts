// Явное приведение к типу
let revenue: number = 1000;
let bonus: number = 500;

// Функции
const getFullName = (userEntity: { firstname: string,  surname: string}): string => {
    return `${userEntity.firstname} ${userEntity.surname}`;
}

const user = {
    firstname: 'Даниил' ,
    surname: 'Луценко',
    city: 'Петрозаводск',
    age: 22,
    skills: {
        dev: true,
        design: false
    }
}

// Массивы 
const skills = ['Dev', 'Devops']

// Tuples
// При попытке добавить что-то отличное от строки -- будет ошибка
// Необходимо расширить тип для переменной softs
// Самое простое решение -- добавить any, но проблема, что использовать можно всё что угодно
const softs: string[] = ['Win', 'Avast', 'VSCode']
