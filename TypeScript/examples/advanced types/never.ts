// TS говорит, что функция вернёт void, но это не совсем так
// Она никогда не возвращает
function generateError(message: string): never {
    throw new Error(message)
}

function dumpError(): never {
    while(true) {
        
    }
}

// Аналогично для рекурсии
// По умолчанию TS поставит any, но мы никогда не вернёмся
// Поэтому логичнее поставить never
function rec() : never {
    return rec()
}

// Никогда ничего не сможем присвоить
// В этом заключается большое отличие never от void
const a: never = null;

// Возвомжное присвоение
const a: void = undefined;

type paymentAction = 'refund' | 'checkout' | 'reject';

// При добавлении нового action получим ошибку в runTime!

function processAction(action: paymentAction) {
    switch (action) {
        case 'refund':
            // ...
            break;
        case 'checkout':
            // ...
            break;
        case 'reject':
            // ...
            break;
        default:
            // _ -- ts не будет ругаться на неиспользуемую переменную
            // Но при добавлении нового action -- будет ошибка
            // Обеспечение реальной ошибки
            const _: never = action; 
            throw new Error('Action not found');
    }
}


// 2. Исчерпывающая проверка
function isString(x: string | number): boolean {
    // Проверка на строку
    if (typeof x === 'string') {
        return true
    // Если оставим только else -- все будет в порядке
    // Но добавив проверку на число -- получим ошибку
    } else if (typeof x === 'number') {
        return false;
    } 
    // Так как есть return undefined в непроверенных случаях
    // Эта функция возвращает never -- никогда сюда не попадём
    generateError('sds');
}