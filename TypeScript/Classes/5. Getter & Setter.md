#### Getter & Setter

Позволяют переопределить как будут получаться или переопределяться свойства нашего объекта

Getter & Setter должны отличаться по названию от свойства, к которому будем что-либо присваивать

```typescript
class User {
  _login: string;
  password: string;

  // Один из вариантов
  // getLogin(l: string){
  //     this.login = 'user-' + l;
  // }

  set login(l) {
    this._login = 'user-' + 1;
  }

  get login() {
    return '';
  }
}

const user = new User();
user.login = 'myLogin';
// log -- User: {_login: 'user-myLog'}
```

\_name -- чтобы свойство отличалось от названия get/set

Getter & Setter позволяют дополнить логику получения и присвоения свойства в объекте

Ограничения Getter & Setter:

1. Если явно не указать тип получаемого значения в setter, то он будет ставится автоматически из return type getter'a

2. Можно указать параметру union type. В таком случае и get и set будут работать с этим unio type

3. Если используем только getter, то свойство, к которому мы обращаемся внутри getter, автоматически станет readonly

4. Getter & Setter не могут быть асинхронными. Проблема заключается в том, что внутри set/get нам придется делать синхронные методы, что будет блокировать основной поток JS.
   Лучше использовать async метод.
