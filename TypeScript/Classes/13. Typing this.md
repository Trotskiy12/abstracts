#### Типизация this

Специальный тип this:
```typescript
class UserBuilder {
    name: string;

    setName(name: string): this { // UserBuilder 
        this.name = name;
        return this;
    }
}

// res type is UserBuilder -- return this;
const res = new UserBuilder().setName('Test')
```

Новый класс, который будет наследоваться от UserBuilder.

При инстанциировании класса, мы получим, что тип **admin** -- будет AdminBuilder, так как в setName мы возвращаем контекст.

```typescript
class AdminBuilder extends UserBuilder {
    roles: string[];
}

// res type is UserBuilder -- return this;
const res = new UserBuilder().setName('Test');
// admin is AdminBuilder -- так как this смотрит на новый контекст, который наследуется
const admin = new AdminBuilder().setName('2');
```

TypeGuard с использованием this.

Чтобы проверить к какому контексту принадлежит инстанс класса используем **this is AdminBuilder**:

```typescript
...
isAdmin(): this is AdminBuilder {
    return this instanceof AdminBuilder;
}
...

let user: UserBuilder | AdminBuilder = new UserBuilder();

if (user.isAdmin()) {
    console.log(user); // AdminBuilder
} else {
    console.log(user); // UserBuilder
}
```

Так же стоит отметить, если в классе AdminBuilder не будет определено свойств/методов, то AdminBuilder будет полностью идентичен классу UserBuilder. 
В таком случае проврека покажет следущее: 

```typescript
...
isAdmin(): this is AdminBuilder {
    return this instanceof AdminBuilder;
}
...

...
class AdminBuilder extends UserBuilder {}
...

let user: UserBuilder | AdminBuilder = new UserBuilder();

if (user.isAdmin()) { 
    console.log(user); // UserBuilder | AdminBuilder
} else {
    console.log(user); // never
}
```
