#### Extends (Наследование)

Имплиментация -- создание прообраза
Наследование -- перенимаем свойства и методы.

```typescript
type PaymentStatus = 'new' | 'paid';

class Payment {
  id: number;
  status: PaymentStatus = 'new';

  constructor(id: number) {
    this.id = id;
  }

  pay() {
    this.status = 'paid';
  }
}

// Все свойства класса Payment + PersistedPayments
class PersistedPayment extends Payment {
  databaseId: number;
  paidAt: Date;
  // Обязательно нужно вызвать конструктор класса от которого наследуем
  constructor() {
    const id = Math.random();
    // Обращаемся к конструктору класса Payment
    super(id);
  }

  save() {
    // save to database
  }
}
```

Override методов.
Если мы переопредялем метод класса, от которого наследуемся. Без указания override будет не понятно собственный ли этот метод или наследуемый?
В случае удаление переопределяемого метода -- ошибки не возникнет. Если будет указан override -- появится ошибка, которая означает, что мы не можем делать override на метод, которого не существует.

```typescript
class PersistedPayment extends Payment {
  databaseId: number;
  paidAt: Date;
  // Обязательно нужно вызвать конструктор класса от которого наследуем
  constructor() {
    const id = Math.random();
    // Обращаемся к конструктору класса Payment
    super(id);
  }

  save() {
    // save to database
  }
  // Сделав дату необязательной - мы можем переопределить метод, так как мы его расширяем
  // Override метода
  // override -- говорит, что мы переопределили метод pay
  override pay(date?: Date) {
    super.pay();
    if (date) {
      this.paidAt = date;
    }
  }
}
```

Использование override улучшит Type Savety.
