#### Видимость свойств

- Публичное свойство:

  public -- свойство можно изменять

  ```typescript
  class Vehicle {
    public mark: string; // публичное свойство
  }
  // Ограничений нет
  new Vehicle().mark = 'audi';
  ```

- Приватное свойство:
  Можем изменять только внутри класса
  Приватные свойства не доступны в классе, который наследуется от базового

```typescript
  class Vehicle {
    public mark: string;
    private damages: string[];
  }
  new Vehicle().mark = 'audi';
  new Vehicle().damages = // error
```

- Protected свойство:
  Также можем обратиться только внутри класса.
  Protected свойства не будут доступны из вне.
  Доступно наследникам.

  ```typescript
  class Vehicle {
    public mark: string;
    private damages: string[];
    private _model: string;
    protected run: number;

    set model(m: string) {
      this._model = m;
    }

    get model() {
      return this._model;
    }

    addDamage(damage: string) {
      this.damages.push(damage);
    }
  }
  class EuroTrack extends Vehicle {
    setRun(km: number) {
      this.run = km / 0.62;
      // this.damages - error;
    }
  }
  ```

Также эти ключевые слова доступны и для методов

В JS #price -- приватное свойство

```typescript
class Vehicle {
  public mark: string;
  private damages: string[];
  private _model: string;
  protected run: number;
  #price: number; //private JS

  set model(m: string) {
    this._model = m;
    this.#price = 100; // можем
  }

  get model() {
    return this._model;
  }
  // Проверка эквивалентонсти приватных свойств
  isPriceEqual(v: Vehicle) {
    return this.#price === v.#price;
  }

  addDamage(damage: string) {
    this.damages.push(damage);
  }
}

class EuroTrack extends Vehicle {
  setRun(km: number) {
    this.run = km / 0.62;
    // this.#price -- не можем
  }
}

new Vehicle().mark = 'audi';
```
